#pragma once
#include <string>
#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe {

private:
	// fields
	char m_board[11] = {' ', '_', '_', '_', '_', '_', '_', '_', '_', '_' };
	int m_numTurns = 0;
	char m_playerTurn = 'X';
	char m_winner;

public:

	// constructor
	TicTacToe() {
		// fields
		m_board;
		m_numTurns;
		m_playerTurn;
		m_winner;
	}

	// methods
	void DisplayBoard() { cout << m_board; }

	bool IsOver() {
		if (m_numTurns == 9) {
			return true;
		}
		else {
			return false;
		}
	}

	char GetPlayerTurn() { return m_playerTurn; }

	bool IsValidMove(int position) {
		if (m_board[position] == '_') {
			return true;
		}
		else {
			cout << "Spot is Occupied! \n";
			return false;
		}
	}

	void Move(int position) { 

		if (m_playerTurn == 'X') {
			m_board[position] = 'X';
			m_playerTurn = 'O';
		}

		else {
			m_board[position] = 'O';
			m_playerTurn = 'X';
			
		}

		m_numTurns++;
	}

	void DisplayResult() { 
		if (m_winner == 'X') {
			cout << "The Winner Is: " + m_winner;
		}

		if (m_winner == 'O') {
			cout << "The Winner Is: " + m_winner;
		}

		else {
			cout << " GAME OVER. IT WAS A TIE \n";
		}
	}
};
